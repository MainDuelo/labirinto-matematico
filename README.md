# Labirinto Matemático

- Scripts

- Testes

- [Demosntração](https://main-duelo.itch.io/labirinto-matematico?secret=L6GwbilLfsEUoX8sFEMr5i5Dv4)

## Documentação

### Unity

- [Unity](https://docs.unity3d.com/Manual/index.html)

- [NUnit](https://nunit.org/documentation/)

- [Unity Test Runner](https://docs.unity3d.com/Manual/testing-editortestsrunner.html)

- [TextMesh Pro](https://docs.unity3d.com/Packages/com.unity.textmeshpro@1.3/manual/index.html)

### Gerar PDF para pontuação

- [iTextSharp](http://afterlogic.com/mailbee-net/docs-itextsharp/)

### Enviar arquivo e texto pelo e-mail

- [System.Net](https://docs.microsoft.com/en-us/dotnet/api/system.net?view=netframework-4.7.2)

- [System.Net.Mail](https://docs.microsoft.com/en-us/dotnet/api/system.net.mail?view=netframework-4.7.2)

## Unity Assets

- [20 Creatures MegaPack](https://assetstore.unity.com/packages/3d/characters/creatures/20-creatures-megapack-79548)

- [Realistic Effects Pack 4](https://assetstore.unity.com/packages/vfx/particles/spells/realistic-effects-pack-4-85675)

- [Mesh Effects](https://assetstore.unity.com/packages/vfx/particles/spells/mesh-effects-67803)

- [Priest Pack](https://assetstore.unity.com/packages/3d/characters/humanoids/priest-pack-126250)

- [TextMesh Pro](https://assetstore.unity.com/packages/essentials/beta-projects/textmesh-pro-84126)


## Repositorios relacionados

- [Gerando tabelas em formato PDF com iTextSharp](https://github.com/MainDuelo/Generating-PDF-tables-with-iTextSharp)

