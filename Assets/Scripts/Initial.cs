﻿using UnityEngine;

public class Initial : MonoBehaviour
{
    void Start()
    {
        GameObject player = Skin.Instance.getCurrentSkin();
        player.transform.localScale = new Vector3(1f,1f, 1f);
        player.GetComponent<PlayerControll>().speed = 4f;
    }
}
