﻿using UnityEngine;
using UnityEngine.UI;

public class EnterPortal : MonoBehaviour
{
    [SerializeField]
    private GameObject levels;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PlayerControll.PLAYER_TAG))
        {
            updateLevelBloqued();
            levels.SetActive(true);
        }
    }

    public void updateLevelBloqued()
    {
        DataBaseScoreController.Instance.connect();
        int level = DataBaseScoreController.Instance.getLevel();
        GameObject _object = GameObject.Find("Canvas").transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).transform.GetChild(0).gameObject;
        if (level > 0)
        {
            _object.transform.GetChild(1).GetComponentInChildren<Text>().text = "II";
            _object.transform.GetChild(1).GetComponentInChildren<Button>().interactable = true;
        }
        if (level > 1)
        {
            _object.transform.GetChild(2).GetComponentInChildren<Text>().text = "III";
            _object.transform.GetChild(2).GetComponentInChildren<Button>().interactable = true;
        }
        if (level > 2)
        {
            _object.transform.GetChild(3).GetComponentInChildren<Text>().text = "IV";
            _object.transform.GetChild(3).GetComponentInChildren<Button>().interactable = true;
        }
        if (level > 3)
        {
            _object.transform.GetChild(4).GetComponentInChildren<Text>().text = "V";
            _object.transform.GetChild(4).GetComponentInChildren<Button>().interactable = true;
        }
        if (level > 4)
        {
            _object.transform.GetChild(5).GetComponentInChildren<Text>().text = "VI";
            _object.transform.GetChild(5).GetComponentInChildren<Button>().interactable = true;
        }
        if (level > 5)
        {
            _object.transform.GetChild(6).GetComponentInChildren<Text>().text = "VII";
            _object.transform.GetChild(6).GetComponentInChildren<Button>().interactable = true;
        }
    }
}
