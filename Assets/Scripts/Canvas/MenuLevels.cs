﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuLevels : MonoBehaviour
{
    [SerializeField]
    private Button buttonClose;
    [SerializeField]
    private Button buttonLevel01, buttonLevel02, buttonLevel03, buttonLevel04,
                   buttonLevel05, buttonLevel06, buttonLevel07;
    [SerializeField]
    private GameObject levels;
    void Start()
    {
        buttonClose.onClick = new Button.ButtonClickedEvent();
        buttonClose.onClick.AddListener(() => clickClose());

        buttonLevel01.onClick = new Button.ButtonClickedEvent();
        buttonLevel01.onClick.AddListener(() => clickLevel01());

        buttonLevel02.onClick = new Button.ButtonClickedEvent();
        buttonLevel02.onClick.AddListener(() => clickLevel02());

        buttonLevel03.onClick = new Button.ButtonClickedEvent();
        buttonLevel03.onClick.AddListener(() => clickLevel03());

        buttonLevel04.onClick = new Button.ButtonClickedEvent();
        buttonLevel04.onClick.AddListener(() => clickLevel04());

        buttonLevel05.onClick = new Button.ButtonClickedEvent();
        buttonLevel05.onClick.AddListener(() => clickLevel05());

        buttonLevel06.onClick = new Button.ButtonClickedEvent();
        buttonLevel06.onClick.AddListener(() => clickLevel06());

        buttonLevel07.onClick = new Button.ButtonClickedEvent();
        buttonLevel07.onClick.AddListener(() => clickLevel07());
    }

    private void clickClose()
    {
        levels.SetActive(false);
    }

    private void clickLevel01()
    {
        DesignMaze.levelName = MazeLevel01.LEVEL_NAME;
        LoadSceneMode();
    }

    private void clickLevel02()
    {
        DesignMaze.levelName = MazeLevel02.LEVEL_NAME;
        LoadSceneMode();
    }

    private void clickLevel03()
    {
        DesignMaze.levelName = MazeLevel03.LEVEL_NAME;
        LoadSceneMode();
    }

    private void clickLevel04()
    {
        DesignMaze.levelName = MazeLevel04.LEVEL_NAME;
        LoadSceneMode();
    }

    private void clickLevel05()
    {
        DesignMaze.levelName = MazeLevel05.LEVEL_NAME;
        LoadSceneMode();
    }

    private void clickLevel06()
    {
        DesignMaze.levelName = MazeLevel06.LEVEL_NAME;
        LoadSceneMode();
    }

    private void clickLevel07()
    {
        DesignMaze.levelName = MazeLevel07.LEVEL_NAME;
        LoadSceneMode();
    }

    private void LoadSceneMode()
    {
        SceneManager.LoadScene("Labirinto");
    }
}
