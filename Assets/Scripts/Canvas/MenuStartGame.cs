﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuStartGame : MonoBehaviour
{
    public const string PLAYER_PREFS_NAME_USER = "NAME_USER";
    public static string NAME_USER;

    [SerializeField]
    private GameObject startGame;
    [SerializeField]
    private Button buttonFinish;
    [SerializeField]
    private Toggle toggleAcceptTermsLicense;
    [SerializeField]
    private InputField InputFieldNameUser;
    [SerializeField]
    private Text textError;
    [SerializeField]
    private TextMeshProUGUI textMeshProUGUIEmail, textMeshProUGUIGeneratePDF, textMeshProUGUIReset;

    private void Awake()
    {
        buttonFinish.onClick = new Button.ButtonClickedEvent();
        buttonFinish.onClick.AddListener(() => clickFinalizar());

        if (existUser())
        {
            disableMenuStartGame();
            NAME_USER = PlayerPrefs.GetString(PLAYER_PREFS_NAME_USER);
            alterTextDialogue();
        }  
    }

    private void clickFinalizar()
    {
        string error = posibleError();

        if (error == null)
        {
            saveName(InputFieldNameUser.text);
            alterTextDialogue();
            startGame.SetActive(false);
        }
        else
            textError.text = error;
    }

    private string posibleError()
    {
        if (InputFieldNameUser.text.Equals(""))
            return "Campo 'Nome' Obrigatorio";
        else if(toggleAcceptTermsLicense.isOn.Equals(false))
            return "Para continuar é obrigatorio aceitar os termos";
        return null;
    }

    private void saveName(string nameUser)
    {
        PlayerPrefs.SetString(PLAYER_PREFS_NAME_USER, nameUser);
        NAME_USER = nameUser;
    }

    private void disableMenuStartGame()
    {
        if(existUser())
            startGame.SetActive(false);
    }

    private bool existUser()
    {
        return PlayerPrefs.HasKey(PLAYER_PREFS_NAME_USER);
    }

    private void alterTextDialogue()
    {
        textMeshProUGUIEmail.SetText("Olá " + NAME_USER + " gostaria de enviar sua pontuação ?");
        textMeshProUGUIGeneratePDF.text = "O que deseja " + NAME_USER + " fazer com a pontuação ?";
        textMeshProUGUIReset.SetText("Oopa " + NAME_USER + " deseja realmente recomeçar o jogo ?");
    }
}
