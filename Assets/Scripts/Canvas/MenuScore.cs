﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScore : MonoBehaviour
{
    [SerializeField]
    private Button butonClose;
    [SerializeField]
    private GameObject score;
    [SerializeField]
    private GameObject line;
    [SerializeField]
    private GameObject content;

    private void Start()
    {
        DataBaseScoreController.Instance.connect();
        butonClose.onClick = new Button.ButtonClickedEvent();
        butonClose.onClick.AddListener(() => clickClose());
        viewScore();
    }

    private void clickClose()
    {
        score.SetActive(false);
    }

    private void viewScore()
    {
        List<LevelScore> levelScores = DataBaseScoreController.Instance.consultAll();

        foreach(LevelScore levelScore in levelScores)
        {
            GameObject cloneLine = Instantiate(line) as GameObject;
            cloneLine.SetActive(true);
            cloneLine.transform.GetChild(0).gameObject.GetComponent<Text>().text = levelScore.level;
            cloneLine.transform.GetChild(1).gameObject.GetComponent<Text>().text = levelScore.time;
            cloneLine.transform.GetChild(2).gameObject.GetComponent<Text>().text = levelScore.correctPlane;
            cloneLine.transform.GetChild(3).gameObject.GetComponent<Text>().text = levelScore.neutralPlane;
            cloneLine.transform.GetChild(4).gameObject.GetComponent<Text>().text = levelScore.badPlane;
            cloneLine.transform.GetChild(5).gameObject.GetComponent<Text>().text = levelScore.terriblePlane;

            cloneLine.transform.SetParent(content.transform);
            cloneLine.transform.localScale = new Vector3Int(1,1,1);
        }

    }
}
