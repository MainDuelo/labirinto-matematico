﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndMaze : MonoBehaviour
{
    [SerializeField]
    private Button buttonNextLevel;
    [SerializeField]
    private Button buttonHome;

    private void Start()
    {
        buttonNextLevel.onClick = new Button.ButtonClickedEvent();
        buttonNextLevel.onClick.AddListener(() => clickNextLevel());

        buttonHome.onClick = new Button.ButtonClickedEvent();
        buttonHome.onClick.AddListener(() => clickHome());
    }

    private void clickNextLevel()
    {
        FindObjectOfType<DesignMaze>().startNextlevel();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void clickHome()
    {
        GameObject player = Skin.Instance.getCurrentSkin();
        player.transform.localScale = new Vector3(1f, 1f, 1f);
        player.GetComponent<PlayerControll>().speed = 4f;
        SceneManager.LoadScene("Home");
    }
}
