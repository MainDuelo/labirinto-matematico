﻿using UnityEngine;

public class MazeLevel03 : MazeLevel
{
    public const string LEVEL_NAME = "03";

    public override bool checkLevel(string level)
    {
        return level.Equals(LEVEL_NAME);
    }

    public override string[,] getRendering()
    {
        line.Add("x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|n|x|x|k|o|o|x|x|x|x|x|x|x|k|o|o|o|o|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|k|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|o|n|n|x|x|x|x|o|o|o|o|x|x|x|x|x|o|x|x|o|o|o|o|o|o|o|x|x|x|x|x|x|o|o|o|o|o|o|o|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|k|o|o|o|x|n|x|x|x|x|o|x|x|x|o|o|o|o|o|o|x|x|o|x|o|x|x|x|o|x|x|o|o|o|o|o|x|x|o|x|x|o|o|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|o|x|x|x|n|x|x|x|o|o|o|o|o|o|x|o|x|x|o|x|x|o|o|o|o|o|o|o|o|o|o|x|x|x|o|x|o|o|o|o|o|x|x|x|o|k|x".Split('|'));
        line.Add("x|x|x|o|o|o|x|n|n|o|o|o|x|x|x|x|o|o|o|o|o|o|x|x|x|o|x|x|x|x|x|x|x|o|x|x|x|o|o|o|x|x|x|x|x|x|x|x|x|x".Split('|'));
        line.Add("x|k|o|o|x|o|x|n|x|x|x|o|x|x|x|x|o|x|x|o|x|o|x|x|x|o|o|o|o|o|o|o|o|o|o|o|o|o|x|x|x|x|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|x|o|x|x|o|n|n|n|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|x|o|x|x|x|x|x|x|x|x|o|o|o|k|x|x|x|x|o|x|x|k|x".Split('|'));
        line.Add("x|o|o|x|x|x|x|n|x|x|x|x|x|o|x|x|x|x|o|x|x|x|x|o|o|o|o|x|o|x|x|x|o|o|o|o|x|o|x|x|x|x|x|o|o|o|o|o|o|x".Split('|'));
        line.Add("x|o|x|x|x|x|x|n|n|o|o|o|o|o|x|x|x|o|o|o|o|x|x|o|x|x|o|o|o|o|o|o|o|x|x|o|x|o|x|x|x|x|x|o|x|x|x|x|x|x".Split('|'));
        line.Add("x|o|o|o|o|x|x|n|x|x|o|x|x|o|o|o|o|o|x|x|o|o|o|o|o|o|x|x|x|x|x|x|o|x|x|o|o|o|x|x|x|o|o|o|o|o|o|o|k|x".Split('|'));
        line.Add("x|x|x|x|o|o|n|n|n|o|o|o|o|o|x|x|x|o|x|x|o|x|o|x|x|o|o|o|o|o|o|o|o|o|o|o|x|x|x|x|x|x|x|o|x|x|o|x|x|x".Split('|'));
        line.Add("x|x|x|x|o|x|x|n|x|o|x|o|o|o|x|x|x|o|x|x|o|x|o|x|x|o|x|x|x|x|x|x|o|x|x|o|o|x|x|o|o|o|o|o|x|k|o|o|k|x".Split('|'));
        line.Add("x|x|x|x|o|x|t|n|x|o|o|o|x|o|o|o|o|o|o|o|o|x|o|x|x|o|x|x|x|x|o|o|o|o|o|x|o|x|x|x|x|k|x|o|x|x|o|x|x|x".Split('|'));
        line.Add("x|o|o|o|o|n|n|x|x|x|x|o|x|o|x|x|x|o|x|x|o|x|o|o|o|o|o|x|x|x|o|x|x|x|x|x|o|o|o|o|x|x|x|o|x|x|o|x|x|x".Split('|'));
        line.Add("x|o|x|o|x|x|n|x|x|o|o|o|x|o|o|o|o|o|o|o|o|o|o|x|o|x|o|x|x|x|o|x|x|x|x|x|x|x|x|o|o|o|o|o|o|o|o|x|x|x".Split('|'));
        line.Add("x|o|x|o|x|x|n|n|o|o|x|x|x|x|o|x|x|x|x|o|x|x|o|o|o|o|o|o|o|o|o|o|x|x|x|o|o|o|x|o|x|x|x|o|x|x|o|x|k|x".Split('|'));
        line.Add("x|o|o|o|o|n|n|x|x|o|x|x|x|x|o|o|o|o|o|o|o|o|o|x|x|o|x|o|x|x|x|x|x|x|x|o|x|o|x|o|o|o|o|o|x|x|o|x|o|x".Split('|'));
        line.Add("x|x|o|x|x|x|n|x|x|o|o|o|o|o|o|o|x|o|x|x|x|x|o|x|x|o|x|o|x|x|x|x|x|o|o|o|o|o|x|o|o|o|o|o|o|o|o|o|o|x".Split('|'));
        line.Add("x|x|o|x|o|n|n|n|o|o|x|x|x|x|x|o|x|o|x|x|x|x|o|x|x|o|x|o|o|o|x|x|x|o|x|x|x|o|o|o|x|x|x|x|o|x|x|x|x|x".Split('|'));
        line.Add("x|x|o|x|o|x|n|x|x|o|x|x|x|x|x|k|x|o|x|x|x|x|o|x|x|o|x|x|x|o|x|x|x|o|x|x|x|o|x|o|o|o|x|x|o|x|x|x|x|x".Split('|'));
        line.Add("x|x|o|o|o|n|n|x|x|o|x|x|x|x|x|x|x|o|x|x|x|x|o|o|o|o|o|x|x|o|o|o|o|o|o|o|o|o|x|x|x|o|k|x|o|o|k|x|x|x".Split('|'));
        line.Add("x|x|x|x|o|x|n|x|x|o|o|o|o|o|o|o|o|o|o|o|o|o|o|x|x|x|x|x|x|x|x|x|x|o|x|o|x|x|x|x|x|o|x|x|o|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|o|x|n|x|x|o|x|x|x|x|x|o|x|o|x|x|x|x|o|x|x|x|x|x|o|x|x|x|x|o|x|o|o|o|o|o|o|o|o|o|o|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|n|t|n|n|o|o|o|o|o|x|x|o|x|o|x|x|x|x|o|o|o|o|x|x|o|o|o|o|o|o|o|o|x|x|x|o|x|x|x|x|o|o|o|o|k|x".Split('|'));
        line.Add("x|x|k|o|x|n|x|x|x|o|x|x|o|x|x|o|x|o|x|x|x|x|x|x|x|o|x|x|o|x|x|x|x|x|x|o|x|x|o|o|x|x|x|k|x|x|x|o|x|x".Split('|'));
        line.Add("x|x|x|o|x|n|x|x|x|o|x|x|o|o|o|o|o|o|o|o|x|x|x|x|x|o|o|o|o|o|o|o|o|o|o|o|x|x|o|x|x|x|x|o|x|x|x|o|x|x".Split('|'));
        line.Add("x|x|x|o|x|n|n|o|o|o|o|o|o|x|x|x|x|x|x|o|o|o|o|x|x|x|o|x|x|x|o|x|x|x|x|o|o|o|o|o|o|o|o|o|o|o|o|o|k|x".Split('|'));
        line.Add("x|x|k|o|n|n|x|x|x|o|x|x|o|x|x|x|x|x|x|x|x|x|o|o|o|o|o|o|o|o|o|o|o|o|o|o|x|x|x|x|o|x|x|x|o|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|n|n|o|o|o|x|x|o|x|x|x|x|x|x|x|x|x|x|x|x|o|x|x|x|o|x|x|o|x|x|o|x|x|x|x|o|x|x|x|o|k|x|x|x|x".Split('|'));
        line.Add("x|x|k|o|n|n|x|x|x|o|o|o|o|o|o|x|x|x|x|x|x|o|o|o|o|o|o|o|o|o|o|o|o|x|x|o|o|o|o|o|o|x|x|x|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|n|x|x|x|o|x|o|x|x|o|o|o|x|x|x|x|x|x|x|o|o|x|x|x|o|x|x|o|x|x|o|x|x|x|x|o|x|x|x|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|o|o|n|n|n|o|o|o|x|o|x|x|o|x|x|x|x|o|o|o|o|x|x|o|o|o|o|o|o|o|o|x|x|o|x|x|x|x|o|o|o|k|x|x|x|x|x|x".Split('|'));
        line.Add("x|k|o|n|t|n|x|x|x|o|x|o|x|x|x|x|x|x|x|x|x|x|o|o|o|o|x|x|x|x|x|x|o|o|o|o|o|o|o|o|o|x|x|x|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|n|x|o|x|x|o|o|o|o|o|x|x|x|o|o|x|x|x|o|x|x|x|x|x|x|x|x|x|o|x|x|x|x|x|x|x|o|o|o|o|o|o|o|o|k|x".Split('|'));
        line.Add("x|o|o|n|n|x|o|o|o|o|o|o|x|o|o|x|x|x|o|o|o|o|o|o|o|o|o|x|x|x|x|x|o|x|x|x|x|x|x|x|x|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|o|x|x|n|n|o|o|o|o|o|o|x|x|o|o|o|x|x|x|x|o|x|x|x|x|o|o|o|o|o|o|o|x|x|x|x|x|x|x|x|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|o|x|x|n|x|x|o|x|x|x|x|x|x|x|x|o|o|o|o|o|o|x|x|x|x|o|x|x|x|x|x|o|o|o|o|o|o|o|o|o|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|k|x|x|n|x|x|o|x|x|x|x|x|x|x|x|o|x|x|x|x|o|o|o|o|x|o|x|x|x|x|x|o|x|x|x|x|o|x|x|x|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|n|n|o|o|x|x|x|x|x|x|x|x|o|x|x|x|o|o|x|x|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|x|x|o|o|o|o|o|x|x|x|x".Split('|'));
        line.Add("x|x|x|t|n|x|o|o|x|x|x|o|o|o|o|o|o|o|o|o|o|x|x|x|o|x|x|x|x|x|o|x|x|x|x|x|x|x|o|x|x|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|x|x|n|x|o|o|x|x|x|x|x|x|x|o|x|o|x|o|x|o|x|x|x|o|x|x|x|x|x|o|x|x|x|x|x|x|x|o|x|x|o|x|o|o|o|o|o|k|x".Split('|'));
        line.Add("x|x|x|n|n|o|x|x|x|o|x|x|x|x|o|x|o|x|o|x|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|x|x|o|x|x|o|x|x".Split('|'));
        line.Add("x|o|n|n|x|x|x|x|x|o|x|x|x|x|o|x|o|x|o|o|o|x|x|o|x|x|x|o|x|x|k|x|x|x|x|o|x|x|o|x|x|x|o|o|o|x|x|o|x|x".Split('|'));
        line.Add("x|x|x|n|x|x|x|x|x|o|x|x|o|o|o|o|o|x|o|x|o|x|x|o|x|x|x|o|x|k|x|x|x|k|o|o|o|o|o|x|x|x|x|x|o|x|x|o|x|x".Split('|'));
        line.Add("x|x|t|n|x|x|x|x|o|o|x|x|o|x|x|o|o|o|o|o|o|o|o|o|x|o|o|o|x|o|x|x|x|x|x|x|x|x|o|x|x|x|x|x|o|o|o|o|o|x".Split('|'));
        line.Add("x|x|n|x|x|o|o|x|o|x|x|x|o|x|x|o|x|x|x|x|x|x|o|o|o|x|x|o|o|o|o|o|o|o|o|o|o|o|o|o|x|x|x|x|x|x|x|x|o|x".Split('|'));
        line.Add("x|n|n|n|x|o|o|o|o|x|x|x|o|o|o|o|o|x|o|o|o|x|o|x|x|x|x|x|x|o|x|x|x|x|x|x|x|x|x|o|o|o|o|x|x|x|o|o|o|x".Split('|'));
        line.Add("x|t|n|o|o|o|o|x|o|o|o|o|o|x|x|x|k|x|x|x|o|o|o|o|o|o|o|o|o|o|k|x|x|x|x|x|x|x|x|x|x|x|o|o|o|o|o|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x".Split('|'));

        line.Reverse();

        return getArray(line);
    }

    public override Vector3 getPosition()
    {
        return new Vector3(1, -0.007f, 1);
    }

    public override Quaternion getRotation()
    {
        return Quaternion.Euler(0, 50, 0);
    }

    public override string nextLevel()
    {
        return MazeLevel04.LEVEL_NAME;
    }

    public override WallPoint getStartWall()
    {
        return new WallPoint(new Vector3(1, 0, 0), new Vector3(0, 0, 1));
    }

    public override WallPoint getEndWall()
    {
        return new WallPoint(new Vector3(7, 0, 48), new Vector3(6, 0, 49));
    }

    public override string mathematicalContent()
    {
        return "f(x) = x²\n\n(01,01)->(07,47)";
    }

    public override int getXInit()
    {
        return 0;
    }

    public override int getXEnd()
    {
        return 50;
    }

    public override int getYInit()
    {
        return 0;
    }

    public override int getYEnd()
    {
        return 50;
    }

    public override Sprite getImageEnd()
    {
        return Resources.Load<Sprite>("Funcoes/Level03");
    }
}

