﻿using UnityEngine;

public class MazeLevel07 : MazeLevel
{
    public const string LEVEL_NAME = "07";

    public override bool checkLevel(string level)
    {
        return level.Equals(LEVEL_NAME);
    }

    public override string[,] getRendering()
    {
        line.Add("x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|x|x|x|k|o|o|x|x|x|x|x|x|x|k|o|o|o|o|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|k|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|x|x|x|x|x|o|o|o|o|x|x|x|x|x|o|x|x|o|x|o|o|o|o|o|x|x|x|x|x|x|o|o|o|o|o|o|o|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|x|x|x|x|x|o|x|x|x|o|o|o|o|o|o|x|x|o|x|o|x|x|x|o|x|x|o|o|o|o|o|x|x|o|x|x|o|o|x|x|x|x|x".Split('|'));
        line.Add("x|t|n|x|x|x|k|x|x|x|x|o|o|o|o|o|o|x|o|x|x|o|x|x|o|x|o|o|o|o|o|o|o|o|x|x|x|o|x|o|o|o|o|o|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|t|n|x|o|o|o|o|o|o|o|x|x|x|x|o|o|o|o|o|o|x|x|x|x|x|x|x|x|x|x|x|o|x|x|x|o|o|o|x|x|x|x|x|x|x|n|t|x".Split('|'));
        line.Add("x|k|x|t|n|x|x|x|o|x|x|o|x|x|x|x|o|x|x|o|x|o|x|x|x|x|o|o|o|o|o|o|o|o|o|o|o|o|x|x|x|x|x|x|x|x|n|t|x|x".Split('|'));
        line.Add("x|x|x|x|t|n|x|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|x|o|x|o|x|x|x|x|x|x|x|x|o|o|o|k|x|x|x|x|n|t|x|k|x".Split('|'));
        line.Add("x|x|x|x|x|t|n|x|x|x|x|x|x|o|x|x|x|x|o|x|x|x|x|o|o|x|o|x|o|x|x|x|o|o|o|o|x|o|o|o|o|o|o|x|n|t|x|x|x|x".Split('|'));
        line.Add("x|x|x|o|o|n|t|n|x|o|o|o|o|o|x|x|x|o|o|o|o|x|x|o|x|x|o|o|o|o|o|o|o|x|x|o|x|o|x|x|x|x|o|n|t|n|x|x|x|x".Split('|'));
        line.Add("x|x|x|o|o|x|x|t|n|x|o|x|x|o|o|o|o|o|x|x|o|o|o|o|o|x|x|x|x|x|x|x|o|x|x|o|o|o|x|x|x|x|n|t|x|o|o|o|k|x".Split('|'));
        line.Add("x|k|o|o|o|x|x|x|t|n|x|o|o|o|x|x|x|o|x|x|o|x|o|x|x|x|o|o|o|o|o|o|o|o|o|o|x|x|x|x|x|n|t|n|x|x|o|x|x|x".Split('|'));
        line.Add("x|x|x|o|x|x|x|o|n|t|n|x|o|o|x|x|x|o|x|x|o|x|o|x|x|x|x|x|x|x|x|x|o|x|x|o|o|o|o|x|n|t|x|o|x|k|o|o|k|x".Split('|'));
        line.Add("x|x|x|o|x|x|x|o|x|x|t|n|x|o|o|o|o|o|o|o|o|x|o|x|x|x|x|x|x|x|o|o|o|o|o|x|x|x|o|n|t|x|x|o|x|x|o|x|x|x".Split('|'));
        line.Add("x|x|x|o|o|o|o|o|x|x|x|t|n|x|x|x|x|o|x|x|o|x|o|o|o|x|o|x|x|x|o|x|x|x|x|x|x|x|n|t|x|x|x|o|x|x|o|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|o|o|x|o|o|n|t|n|x|o|o|o|o|o|o|o|o|x|o|x|o|x|x|x|o|x|x|x|x|x|x|n|t|x|o|o|o|x|o|o|o|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|o|o|o|o|x|x|x|t|n|x|x|x|x|o|x|x|o|o|o|x|o|o|o|o|o|o|o|o|o|o|n|t|x|o|x|x|x|o|x|x|o|x|k|x".Split('|'));
        line.Add("x|k|o|o|o|o|o|x|x|o|x|x|x|x|t|n|x|o|o|o|o|o|o|x|x|x|x|o|x|x|x|x|x|x|x|n|t|x|x|o|o|o|o|o|x|x|o|x|o|x".Split('|'));
        line.Add("x|x|x|x|x|x|o|x|x|o|o|o|o|o|n|t|n|x|x|x|x|x|o|x|x|x|x|o|x|x|x|x|x|x|n|t|n|o|x|o|o|o|o|x|o|o|o|o|o|x".Split('|'));
        line.Add("x|x|x|x|o|o|o|o|o|o|x|x|x|x|x|x|t|n|x|x|x|x|o|x|x|x|x|o|o|x|x|x|x|n|t|x|x|o|o|o|x|x|x|x|o|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|o|x|x|x|x|o|x|x|x|x|x|x|x|t|n|x|x|x|o|x|x|x|x|x|x|x|x|x|n|t|x|x|x|o|x|o|o|o|x|x|o|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|o|x|x|x|x|o|x|x|x|x|x|x|x|x|t|n|x|x|o|o|o|x|x|x|x|x|x|n|t|n|o|o|o|o|x|x|x|o|k|x|o|o|k|x|x|x".Split('|'));
        line.Add("x|x|x|x|o|x|x|x|x|o|o|o|o|o|o|o|o|o|n|t|n|x|o|x|x|x|x|x|x|x|n|t|x|o|x|o|x|x|x|x|x|x|x|x|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|o|x|x|x|x|o|x|x|x|x|x|o|x|o|x|x|t|x|o|x|x|x|x|x|x|x|t|x|x|o|x|o|o|o|o|o|o|o|o|o|o|x|x|x|o|x".Split('|'));
        line.Add("x|x|x|x|o|x|o|o|o|o|o|o|o|x|x|o|x|o|x|x|n|x|x|o|x|x|x|x|o|x|n|x|o|o|o|x|x|x|x|o|x|x|x|x|o|o|o|o|o|x".Split('|'));
        line.Add("x|x|x|x|o|o|o|x|x|o|x|x|o|x|x|o|x|o|x|x|n|x|x|x|x|x|x|x|o|x|n|x|x|x|x|o|x|x|o|o|x|x|x|k|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|o|x|x|o|x|x|o|o|o|o|o|o|o|n|n|x|x|x|o|x|o|o|o|x|n|n|o|o|o|o|x|x|o|x|x|x|x|o|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|o|o|o|o|o|o|o|x|x|x|x|x|x|x|n|x|x|o|x|x|o|x|x|x|n|x|x|x|x|o|o|o|x|o|o|o|o|o|o|x|x|x|o|x".Split('|'));
        line.Add("x|x|k|o|x|x|x|x|x|o|x|x|o|x|x|x|x|x|x|x|n|x|o|o|o|x|o|o|o|x|n|x|o|o|o|o|x|x|x|x|o|x|x|x|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|x|o|o|x|x|o|x|x|x|x|x|x|x|n|x|x|x|x|x|x|x|x|x|n|x|o|x|x|o|x|x|x|x|o|x|x|x|x|k|x|x|x|x".Split('|'));
        line.Add("x|x|k|o|o|o|x|x|x|o|o|o|o|o|o|x|x|x|x|x|n|x|x|o|o|x|o|o|o|x|n|x|o|x|x|o|o|o|o|o|o|x|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|x|x|x|x|o|x|x|o|o|o|o|x|x|n|x|x|x|o|x|x|x|x|x|n|x|o|x|x|o|x|x|x|x|o|x|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|x|x|o|o|o|o|o|x|x|x|o|x|x|o|x|x|o|o|n|n|t|x|x|x|x|o|o|x|t|n|n|o|x|x|o|x|x|x|x|x|x|x|k|x|o|x|x|x|x".Split('|'));
        line.Add("x|k|o|o|x|x|x|x|x|x|x|x|x|x|x|x|x|o|x|x|x|n|x|o|o|x|x|x|x|n|x|x|o|o|o|o|o|o|o|x|x|x|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|x|x|o|o|o|o|x|x|o|x|x|x|o|o|x|o|o|o|x|x|n|x|x|x|x|x|x|x|n|x|x|o|x|x|x|x|x|x|x|x|o|o|o|o|o|o|o|k|x".Split('|'));
        line.Add("x|o|o|o|o|o|o|o|o|o|o|x|x|x|x|o|x|x|o|o|n|n|x|o|o|x|o|x|x|n|x|x|o|x|x|x|o|o|x|x|x|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|o|x|o|x|x|x|o|x|x|o|o|x|x|x|o|o|x|x|x|x|n|x|x|x|x|o|o|x|n|n|o|o|x|x|o|o|x|x|x|x|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|o|x|o|o|x|x|o|x|x|x|o|x|x|x|x|o|o|o|o|n|n|x|x|x|x|o|x|x|n|x|x|o|o|o|o|x|x|o|o|o|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|k|x|x|o|o|x|o|x|x|x|x|o|x|x|x|x|x|x|x|x|n|x|o|o|x|o|x|x|n|x|x|o|o|o|x|x|o|x|x|x|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|o|o|x|x|x|x|o|x|x|x|x|x|x|x|x|x|n|t|x|o|x|o|x|t|n|x|o|o|o|x|x|o|o|o|x|x|o|o|o|o|o|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|o|o|o|x|o|o|o|o|o|o|x|x|x|o|o|x|n|x|o|x|x|x|n|x|o|o|o|x|x|x|x|x|o|x|x|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|o|o|x|x|o|x|x|x|x|o|x|o|x|x|x|o|n|n|x|o|x|x|x|n|n|o|o|x|x|x|x|x|x|o|x|x|o|x|o|o|o|o|o|k|x".Split('|'));
        line.Add("x|x|x|o|o|o|o|x|o|o|x|x|x|x|o|x|o|x|x|x|x|x|n|x|o|x|o|x|n|x|o|x|x|o|o|o|o|o|o|o|o|o|x|x|o|x|x|o|x|x".Split('|'));
        line.Add("x|k|o|o|o|x|x|o|x|o|x|x|x|x|o|x|o|x|o|x|x|x|n|x|x|x|x|x|n|x|x|x|x|x|x|o|x|x|o|x|x|x|o|o|o|x|x|o|x|x".Split('|'));
        line.Add("x|x|x|o|x|x|o|x|x|o|x|x|o|o|o|o|o|x|o|x|x|x|n|t|x|x|x|t|n|x|x|x|x|k|o|o|o|o|o|x|x|x|x|x|o|x|x|o|x|x".Split('|'));
        line.Add("x|x|x|o|x|o|x|x|o|o|x|x|o|x|x|o|o|o|o|o|o|o|n|n|x|x|x|n|x|x|x|x|x|x|x|x|x|x|o|x|x|x|x|x|o|o|o|o|o|x".Split('|'));
        line.Add("x|x|x|x|o|o|x|x|o|x|x|x|o|x|x|o|x|x|x|x|x|x|x|n|x|x|n|n|n|o|o|o|o|o|o|o|o|o|o|o|x|x|x|x|x|x|x|x|o|x".Split('|'));
        line.Add("x|x|x|o|x|o|o|o|o|x|x|x|o|o|o|o|o|x|o|o|o|x|x|n|t|x|t|n|x|x|x|x|x|x|x|x|x|x|x|o|o|o|o|x|x|x|o|o|o|x".Split('|'));
        line.Add("x|x|o|o|o|o|o|x|o|o|o|o|o|x|x|x|k|x|x|x|x|o|o|x|n|t|n|x|x|o|k|x|x|x|x|x|x|x|x|x|x|x|o|o|o|o|o|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x".Split('|'));

        line.Reverse();

        return getArray(line);
    }

    public override Vector3 getPosition()
    {
        return new Vector3(-24, -0.007f, 44);
    }

    public override Quaternion getRotation()
    {
        return Quaternion.Euler(0, 50, 0);
    }

    public override string nextLevel()
    {
        return MazeLevel01.LEVEL_NAME;
    }

    public override WallPoint getStartWall()
    {
        return new WallPoint(new Vector3(-24, 0, 45), new Vector3(-25, 0, 44));
    }

    public override WallPoint getEndWall()
    {
        return new WallPoint(new Vector3(24, 0, 43), new Vector3(23, 0, 44));
    }

    public override string mathematicalContent()
    {
        return "       |-x +20 : -40<x<-5\nf(x)=|x² : -5<x<5\n       |x+20 :caso contrário\n\n(-24,44)->(23,43)";
    }

    public override int getXInit()
    {
        return -25;
    }

    public override int getXEnd()
    {
        return 25;
    }

    public override int getYInit()
    {
        return -1;
    }

    public override int getYEnd()
    {
        return 49;
    }

    public override Sprite getImageEnd()
    {
        return Resources.Load<Sprite>("Funcoes/Level07");
    }
}
