﻿using UnityEngine;

public class MazeLevel06 : MazeLevel
{
    public const string LEVEL_NAME = "06";

    public override bool checkLevel(string level)
    {
        return level.Equals(LEVEL_NAME);
    }

    public override string[,] getRendering()
    {
        line.Add("x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|x|x|x|k|o|o|x|x|x|x|x|x|x|k|o|o|o|o|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|k|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|x|x|x|x|x|o|o|o|o|x|x|x|x|x|o|x|x|o|o|o|o|o|o|o|x|x|x|x|x|x|o|o|o|o|o|o|o|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|x|x|x|x|x|o|x|x|x|o|o|o|o|o|o|x|x|o|x|o|x|x|x|o|x|x|o|o|o|o|o|x|x|o|x|x|o|o|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|k|x|x|k|x|x|x|x|o|o|o|o|o|o|x|o|x|x|o|x|x|o|o|o|o|o|o|o|o|o|o|x|x|x|o|x|o|o|o|o|o|x|x|x|o|k|x".Split('|'));
        line.Add("x|x|x|o|o|o|o|o|o|o|o|o|x|x|x|x|o|o|o|o|o|o|x|x|x|o|x|x|x|x|x|x|x|o|x|x|x|o|o|o|x|x|x|x|x|x|x|x|x|x".Split('|'));
        line.Add("x|k|o|o|x|o|x|x|o|x|x|o|x|x|x|x|o|x|x|o|x|o|x|x|x|o|o|o|o|o|o|o|o|o|o|o|o|o|x|x|x|x|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|x|o|x|x|x|x|x|x|x|x|o|o|o|k|x|x|x|x|o|x|x|k|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|o|x|x|x|x|x|o|x|x|x|x|o|x|x|x|x|o|o|o|o|x|o|x|x|x|o|o|o|o|x|o|x|x|x|x|x|o|o|o|o|o|o|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|o|o|o|o|o|o|o|x|x|x|o|o|o|o|x|x|o|x|x|o|o|o|o|o|o|o|x|x|o|x|o|x|x|x|x|x|o|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|x|o|x|o|x|x|o|o|o|o|o|x|x|o|o|o|o|o|o|x|x|x|x|x|x|o|x|x|o|o|o|x|x|x|o|o|o|o|o|o|o|k|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|o|o|o|o|o|o|o|x|x|x|o|x|x|o|x|o|x|x|o|o|o|o|o|o|o|o|o|o|o|x|x|x|x|x|x|x|o|x|x|o|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|o|x|o|x|o|o|o|x|x|x|o|x|x|o|x|o|x|x|o|x|x|x|x|x|x|o|x|x|o|x|x|x|o|o|o|o|o|x|k|o|o|k|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|o|x|o|o|o|x|o|o|o|o|o|o|o|o|x|o|x|x|o|x|x|x|x|o|o|o|o|o|x|x|x|x|x|x|k|x|o|x|x|o|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|o|x|x|x|o|x|o|x|x|x|o|x|x|o|x|o|o|o|o|o|x|x|x|o|x|x|x|x|x|x|o|o|o|x|x|x|o|x|x|o|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|o|x|o|o|o|x|o|o|o|o|o|o|o|o|o|o|x|o|x|o|x|x|x|o|x|x|x|x|x|x|x|x|o|o|o|o|o|o|o|o|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|o|o|o|o|x|x|x|x|o|x|x|x|x|o|x|x|o|o|o|o|o|o|o|o|o|o|x|x|x|o|o|o|x|o|x|x|x|o|x|x|o|x|k|x".Split('|'));
        line.Add("x|k|o|o|o|o|o|x|x|o|x|x|x|x|o|o|o|o|o|o|o|o|o|x|x|o|x|o|x|x|x|x|x|x|x|o|x|o|x|o|o|o|o|o|x|x|o|x|o|x".Split('|'));
        line.Add("x|x|x|x|x|x|o|x|x|o|o|o|o|o|o|o|x|o|x|x|x|x|o|x|x|o|x|o|x|x|x|x|x|o|o|o|o|o|x|o|o|o|o|o|o|o|o|o|o|x".Split('|'));
        line.Add("x|x|x|x|o|o|o|o|o|o|x|x|x|x|x|o|x|o|x|x|x|x|o|x|x|o|x|o|o|x|x|x|x|o|x|x|x|o|o|o|x|x|x|x|o|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|o|x|x|x|x|o|x|x|x|x|x|k|x|o|x|x|x|x|o|x|x|o|x|x|x|x|x|x|x|o|x|x|x|o|x|o|o|o|x|x|o|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|o|x|x|x|x|o|x|x|x|x|x|x|x|o|x|x|x|x|o|o|o|o|o|x|x|x|o|o|o|o|o|o|o|o|x|x|x|o|k|x|o|o|k|x|x|x".Split('|'));
        line.Add("x|x|x|x|o|x|x|x|x|o|o|o|o|o|o|o|o|o|o|o|o|o|o|x|x|x|x|x|x|x|x|x|x|o|x|o|x|x|x|x|x|o|x|x|o|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|o|x|x|x|x|o|x|x|x|x|x|o|x|o|x|x|x|x|o|x|x|x|x|x|o|x|x|x|x|o|x|o|o|o|o|o|o|o|o|o|o|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|o|x|o|o|o|o|o|o|o|x|x|o|x|o|x|x|x|x|o|o|x|x|x|x|o|o|o|o|o|o|o|o|x|x|x|o|x|x|x|x|o|o|o|o|k|x".Split('|'));
        line.Add("x|x|k|o|o|o|o|x|x|o|x|x|o|x|x|o|x|o|x|x|x|x|x|x|x|x|x|x|o|x|x|x|x|x|x|o|x|x|o|o|x|x|x|k|x|x|x|o|x|x".Split('|'));
        line.Add("x|x|x|o|x|x|o|x|x|o|x|x|o|o|o|o|o|o|o|o|x|x|x|x|x|o|o|o|o|o|o|o|o|o|o|o|x|x|o|x|x|x|x|o|x|x|x|o|x|x".Split('|'));
        line.Add("x|x|x|o|x|x|o|o|o|o|o|o|o|x|x|x|x|x|x|o|o|x|x|x|x|x|o|x|x|x|o|x|x|x|x|o|o|o|o|o|o|o|o|o|o|o|o|o|k|x".Split('|'));
        line.Add("x|x|k|o|o|o|o|x|x|o|x|x|o|x|x|x|x|x|x|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|x|x|x|x|o|x|x|x|o|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|o|o|o|o|o|x|x|o|x|x|x|x|x|x|o|o|x|x|x|x|o|x|x|x|o|x|x|o|x|x|o|x|x|x|x|o|x|x|x|o|k|x|x|x|x".Split('|'));
        line.Add("x|x|k|o|o|o|x|x|x|o|o|o|o|o|o|x|x|x|o|o|x|o|o|o|o|o|o|o|o|o|o|o|o|x|x|o|o|o|o|o|o|x|x|x|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|x|x|o|x|o|x|x|o|o|o|o|o|x|x|x|x|x|o|o|x|x|x|o|x|x|o|x|x|o|x|x|x|x|o|x|x|x|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|o|o|o|o|o|o|o|x|o|x|x|o|x|o|o|o|o|o|o|o|x|x|o|o|o|o|o|o|o|o|x|x|o|x|x|x|x|o|o|o|k|x|x|x|x|x|x".Split('|'));
        line.Add("x|k|o|o|x|x|x|x|x|o|x|o|x|x|x|o|o|x|x|x|x|x|o|o|o|o|x|x|x|x|x|x|o|o|o|o|o|o|o|o|o|x|x|x|x|x|x|x|x|x".Split('|'));
        line.Add("x|x|x|o|o|o|o|x|x|o|o|o|o|o|o|o|x|o|o|x|x|x|o|x|x|x|x|x|x|x|x|x|o|x|x|x|x|x|x|x|o|o|o|o|o|o|o|o|k|x".Split('|'));
        line.Add("x|o|o|o|o|o|o|o|o|o|o|o|x|o|o|x|x|x|o|o|o|o|o|o|o|o|o|x|x|x|x|x|o|x|x|x|x|x|x|x|x|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|o|x|o|x|x|x|o|x|x|x|o|o|o|o|o|o|x|x|x|x|o|x|x|x|x|o|o|o|o|o|o|o|x|x|x|x|x|x|x|x|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|o|x|o|o|x|x|o|x|x|x|o|o|x|x|x|o|o|o|o|o|o|x|x|x|x|o|x|x|x|x|x|o|o|o|o|o|o|o|o|o|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|k|x|x|o|o|x|o|x|x|x|n|x|x|x|x|n|x|x|x|x|n|x|x|x|x|n|x|x|x|x|x|o|x|x|x|x|o|x|x|x|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|o|o|x|x|n|t|t|t|t|t|t|t|t|t|t|t|t|t|t|t|t|t|t|t|t|t|n|o|o|o|o|o|o|o|x|x|o|o|o|o|o|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|o|o|n|t|n|x|x|x|n|x|n|x|n|x|n|x|x|x|n|x|x|x|x|x|n|t|x|x|x|x|x|x|o|x|x|o|x|x|x|o|x|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|o|o|x|t|n|x|x|x|x|o|x|o|x|o|x|o|x|x|x|o|x|x|x|x|x|x|n|t|x|x|x|x|x|o|x|x|o|x|o|o|o|o|o|k|x".Split('|'));
        line.Add("x|x|x|o|o|o|n|t|n|o|x|x|x|x|o|x|o|x|o|x|o|o|o|o|o|o|o|o|o|o|o|o|n|t|n|o|o|o|o|o|o|o|x|x|o|x|x|o|x|x".Split('|'));
        line.Add("x|k|o|o|o|x|t|n|x|o|x|x|x|x|o|x|o|x|o|o|o|x|x|o|x|x|x|o|x|x|k|x|x|n|t|n|x|x|o|x|x|x|o|o|o|x|x|o|x|x".Split('|'));
        line.Add("x|x|x|o|x|t|n|x|x|o|x|x|o|o|o|o|o|x|o|x|o|x|x|o|x|x|x|o|x|k|x|x|x|x|n|t|n|o|o|x|x|x|x|x|o|x|x|o|x|x".Split('|'));
        line.Add("x|x|x|n|t|n|x|x|o|o|x|x|o|x|x|o|o|o|o|o|o|o|o|o|x|o|o|o|x|o|x|x|x|x|x|n|t|x|o|x|x|x|x|x|o|o|o|o|o|x".Split('|'));
        line.Add("x|x|x|t|n|o|x|x|o|x|x|x|o|x|x|o|x|x|x|x|x|x|o|o|o|x|x|o|o|o|o|o|o|o|o|o|n|t|n|o|x|x|x|x|x|x|x|x|o|x".Split('|'));
        line.Add("x|x|t|n|x|o|o|o|o|x|x|x|o|o|o|o|o|x|o|o|o|x|o|x|x|x|x|x|x|o|x|x|x|x|x|x|x|n|t|n|o|o|o|x|x|x|o|o|o|x".Split('|'));
        line.Add("x|t|n|o|o|o|o|x|o|o|o|o|o|x|x|x|k|x|x|x|o|o|o|o|o|o|o|o|o|o|k|x|x|x|x|x|x|x|n|t|x|x|o|o|o|o|o|x|x|x".Split('|'));
        line.Add("x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x|x".Split('|'));

        line.Reverse();

        return getArray(line);
    }

    public override Vector3 getPosition()
    {
        return new Vector3(1, -0.007f, 1);
    }

    public override Quaternion getRotation()
    {
        return Quaternion.Euler(0, 50, 0);
    }

    public override string nextLevel()
    {
        return MazeLevel07.LEVEL_NAME;
    }

    public override WallPoint getStartWall()
    {
        return new WallPoint(new Vector3(1, 0, 0), new Vector3(0, 0, 1));
    }

    public override WallPoint getEndWall()
    {
        return new WallPoint(new Vector3(39, 0, 0), new Vector3(38, 0, 1));
    }

    public override string mathematicalContent()
    {
        return "       |x : x<10\nf(x)=|10 : 10<x<30\n       |-x+40 :caso contrário\n\n(01,01)->(39,01)";
    }

    public override int getXInit()
    {
        return 0;
    }

    public override int getXEnd()
    {
        return 50;
    }

    public override int getYInit()
    {
        return 0;
    }

    public override int getYEnd()
    {
        return 50;
    }

    public override Sprite getImageEnd()
    {
        return Resources.Load<Sprite>("Funcoes/Level06");
    }
}

