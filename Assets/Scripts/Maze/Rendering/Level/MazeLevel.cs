﻿using System.Collections.Generic;
using UnityEngine;

public abstract class MazeLevel 
{
    protected List<string[]> line = new List<string[]>();

    public abstract bool checkLevel(string level);
    public abstract string[,] getRendering();
    public abstract string nextLevel();
    public abstract string mathematicalContent();
    public abstract Vector3 getPosition();
    public abstract Quaternion getRotation();
    public abstract WallPoint getStartWall();
    public abstract WallPoint getEndWall();
    public abstract int getXInit();
    public abstract int getXEnd();
    public abstract int getYInit();
    public abstract int getYEnd();
    public abstract Sprite getImageEnd();

    protected string[,] getArray(List<string[]> linesMaze)
    {
        string[,] maze = new string[50, 50];
        for (int i = 0; i < 50; i++)
            for (int j = 0; j < 50; j++)
                maze[i, j] = (string)line[i].GetValue(j);

        return maze;
    }
}
