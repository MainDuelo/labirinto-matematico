﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DesignMaze : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI textMeshProUGUIMathematicalContent;
    [SerializeField]
    private GameObject startWallPoint01, startWallPoint02, endWallPoint01, endWallPoint02;
    
    public GameObject plane;
    public GameObject cube;

    public static string levelName = "01";
    private List<MazeLevel> mazeLevels = new List<MazeLevel>();
    private MazeLevel mazeLevel;

    private string[,] maze = new string[50, 50];

    [SerializeField]
    private Image endImage;

    private void Start()
    {
        MazeInfo.Instance.initializedPlanes();
        instantiateLevel();
        setLevel();
        renderingMaze();
        setStartWallPoint();
        setEndWallPoint();
        setMathematicalContent();
        respawnPlayer();
        setImageEnd();
    }

    private void setImageEnd()
    {
        endImage.sprite = mazeLevel.getImageEnd();
    }

    private void setLevel()
    {
        foreach (MazeLevel mazeLevel in mazeLevels)
        {
            if (mazeLevel.checkLevel(levelName))
            {
                this.mazeLevel = mazeLevel;
                break;
            }
        }
    }

    private void renderingMaze()
    {
        int incrementX = 0;
        int incrementY = 0;
        maze = mazeLevel.getRendering();
        for (int i = mazeLevel.getXInit(); i < mazeLevel.getXEnd(); i++)
        {
            for (int j = mazeLevel.getYInit(); j < mazeLevel.getYEnd(); j++)
            {
                if (maze[incrementY, incrementX].Equals("x"))
                {
                    GameObject cube = Instantiate(this.cube);
                    cube.transform.position = new Vector3(i, 0, j);
                }
                else
                {
                    GameObject plane = Instantiate(this.plane);
                    plane.transform.position = new Vector3(i, 0, j);
                    plane.gameObject.transform.GetChild(0).GetChild(0).GetComponent<TextMeshPro>().text = "X=" + i + "\nY=" + j;

                    if (maze[incrementY, incrementX].Equals("o"))
                        plane.gameObject.transform.GetChild(0).gameObject.AddComponent<PlaneBad>();
                    else if (maze[incrementY, incrementX].Equals("n"))
                        plane.gameObject.transform.GetChild(0).gameObject.AddComponent<PlaneNeutral>();
                    else if (maze[incrementY, incrementX].Equals("t"))
                        plane.gameObject.transform.GetChild(0).gameObject.AddComponent<PlaneCorrect>();
                    else if (maze[incrementY, incrementX].Equals("k"))
                        plane.gameObject.transform.GetChild(0).gameObject.AddComponent<PlaneTerrible>();
                }
                incrementY++;
            }
            incrementY = 0;
            incrementX++;
        }
    }

    private void instantiateLevel()
    {
        mazeLevels.Add(new MazeLevel01());
        mazeLevels.Add(new MazeLevel02());
        mazeLevels.Add(new MazeLevel03());
        mazeLevels.Add(new MazeLevel04());
        mazeLevels.Add(new MazeLevel05());
        mazeLevels.Add(new MazeLevel06());
        mazeLevels.Add(new MazeLevel07());
    }

    private void respawnPlayer()
    {
        GameObject player = Skin.Instance.getCurrentSkin();
        player.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
        player.GetComponent<PlayerControll>().speed = 2.2f;
        Instantiate(Skin.Instance.getCurrentSkin(), mazeLevel.getPosition(), mazeLevel.getRotation());
    }

    public void startNextlevel()
    {
        levelName = mazeLevel.nextLevel();
    }

    private void setStartWallPoint()
    {
        startWallPoint01.transform.position = mazeLevel.getStartWall().wallPosition01;
        startWallPoint02.transform.position = mazeLevel.getStartWall().wallPosition02;
    }

    private void setEndWallPoint()
    {
        endWallPoint01.transform.position = mazeLevel.getEndWall().wallPosition01;
        endWallPoint02.transform.position = mazeLevel.getEndWall().wallPosition02;
    }


    private void setMathematicalContent()
    {
        textMeshProUGUIMathematicalContent.text = mazeLevel.mathematicalContent();
    }
}
