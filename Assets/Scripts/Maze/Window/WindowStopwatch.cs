﻿using System;
using TMPro;
using UnityEngine;

public class WindowStopwatch : MonoBehaviour
{
    private bool playing = true;
    private float time;
    [SerializeField]
    private GameObject stopWatch;

    private void Update()
    {
        if (playing)
        {
            time += Time.deltaTime;
            string minutes = Mathf.Floor((time % 3600)/60).ToString("00");
            string seconds = Mathf.Floor(time %  60).ToString("00");
            stopWatch.GetComponent<TextMeshProUGUI>().text = minutes + ":" + seconds;
        }
    }

    public void endStopwatch()
    {
        playing = false;
        MazeInfo.Instance.time = Convert.ToInt32(time);
    }
}
