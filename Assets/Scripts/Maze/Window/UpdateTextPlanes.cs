﻿using TMPro;
using UnityEngine;

public class UpdateTextPlanes : MonoBehaviour
{
    [SerializeField]
    private GameObject correctPlane, neutralPlane, badPlane, terriblePlane;

    void Start()
    {
        correctPlane.GetComponent<TextMeshProUGUI>().text = "0";
        neutralPlane.GetComponent<TextMeshProUGUI>().text = "0";
        badPlane.GetComponent<TextMeshProUGUI>().text = "0";
        terriblePlane.GetComponent<TextMeshProUGUI>().text = "0";

        InvokeRepeating("updateText", 2, 1);
    }

    private void updateText()
    {
        correctPlane.GetComponent<TextMeshProUGUI>().text = MazeInfo.Instance.getCorrectPlane();
        neutralPlane.GetComponent<TextMeshProUGUI>().text = MazeInfo.Instance.getNeutralPlane();
        badPlane.GetComponent<TextMeshProUGUI>().text = MazeInfo.Instance.getBadPlane();
        terriblePlane.GetComponent<TextMeshProUGUI>().text = MazeInfo.Instance.getTerriblePlane();
    }
}
