﻿using UnityEngine;

public class MazeInfo : MonoBehaviour
{
    private int correctPlane;
    private int badPlane;
    private int neutralPlane;
    private int terriblePlane;
    public int time;

    [SerializeField]
    public string level;

    public static MazeInfo Instance { get; private set; }

    private void Awake()
    {
        DataBaseScoreController.Instance.connect();
        initializedPlanes();
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    } 

    public void correctPlaneIncrement()
    {
        correctPlane++;
    }

    public void badPlaneIncrement()
    {
        badPlane++;
    }

    public void neutralPlaneIncrement()
    {
        neutralPlane++;
    }

    public void terriblePlaneIncrement()
    {
        terriblePlane++;
    }

    public string getCorrectPlane()
    {
        return (correctPlane / 2).ToString();
    }

    public string getBadPlane()
    {
        return (badPlane / 2).ToString();
    }

    public string getNeutralPlane()
    {
        return (neutralPlane / 2).ToString();
    }

    public string getTerriblePlane()
    {
        return (terriblePlane / 2).ToString();
    }

    public void initializedPlanes()
    {
        correctPlane = 0;
        badPlane = 0;
        neutralPlane = 0;
        terriblePlane = 0;
    }
}
