﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlaneNeutral : MonoBehaviour
{

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(PlayerControll.PLAYER_TAG)) {
            {
                MazeInfo.Instance.neutralPlaneIncrement();
                transform.GetChild(0).GetComponent<TextMeshPro>().color = Color.white;
                Destroy(GetComponent<PlaneNeutral>());
            }
        }
    }
}
