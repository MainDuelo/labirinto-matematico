﻿using TMPro;
using UnityEngine;

public class PlaneTerrible : MonoBehaviour
{

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(PlayerControll.PLAYER_TAG))
        {
            transform.GetChild(0).GetComponent<TextMeshPro>().color = Color.black;
            MazeInfo.Instance.terriblePlaneIncrement();
            Destroy(GetComponent<PlaneTerrible>());
        }
    }
}
