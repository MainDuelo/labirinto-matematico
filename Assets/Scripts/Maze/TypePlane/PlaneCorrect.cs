﻿using TMPro;
using UnityEngine;

public class PlaneCorrect : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(PlayerControll.PLAYER_TAG))
        {
            MazeInfo.Instance.correctPlaneIncrement();
            transform.GetChild(0).GetComponent<TextMeshPro>().color = Color.green;
            Destroy(GetComponent<PlaneCorrect>());
        }
    }
}
