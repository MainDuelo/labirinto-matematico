﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlaneBad : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(PlayerControll.PLAYER_TAG))
        {
            MazeInfo.Instance.badPlaneIncrement();
            transform.GetChild(0).GetComponent<TextMeshPro>().color = Color.red;
            Destroy(GetComponent<PlaneBad>());
        }
            
    }
}
