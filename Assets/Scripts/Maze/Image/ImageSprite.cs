﻿using UnityEngine;

public class ImageSprite : MonoBehaviour
{
    public Sprite level01, level02, level03;

    public static ImageSprite Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
