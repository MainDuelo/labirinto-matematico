﻿using UnityEngine;

public class ResizeImage : MonoBehaviour
{
    private void Start()
    {
        GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
    }
}
