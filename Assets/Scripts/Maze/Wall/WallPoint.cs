﻿using UnityEngine;

public class WallPoint 
{
    public Vector3 wallPosition01 { get; private set; }
    public Vector3 wallPosition02 { get; private set; }

    public WallPoint(Vector3 wallPosition01, Vector3 wallPosition02)
    {
        this.wallPosition01 = wallPosition01;
        this.wallPosition02 = wallPosition02;
    }
}
