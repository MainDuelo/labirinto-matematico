﻿using UnityEngine;

public class CubeEndCollider : MonoBehaviour
{
    [SerializeField]
    private GameObject endMaze, stopwatch;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(PlayerControll.PLAYER_TAG))
        {
            endMaze.SetActive(true);
            stopwatch.GetComponent<WindowStopwatch>().endStopwatch();
            saveScore();
            collision.rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        }     
    }

    private void saveScore()
    {
        DataBaseScoreController.Instance.updateOrInsert();
    }
}
