﻿
public class LevelScore
{
    public const string LEVEL = "Nível";
    public const string TIME = "Tempo";
    public const string CORRECT_PLANE = "Correto";
    public const string NEUTRAL_PLANE = "Neutro";
    public const string BAD_PLANE = "Ruim";
    public const string TERRIBLE_PLANE = "Péssimo";

    public string level { get; private set; }
    public string time { get; private set; }
    public string correctPlane { get; private set; }
    public string neutralPlane { get; private set; }
    public string badPlane { get; private set; }
    public string terriblePlane { get; private set; }

    public LevelScore setLevel(string level)
    {
        this.level = level;
        return this;
    }

    public LevelScore setTime(string time)
    {
        this.time = time;
        return this;
    }

    public LevelScore setCorrectPlane(string correctPlane)
    {
        this.correctPlane = correctPlane;
        return this;
    }

    public LevelScore setNeutralPlane(string neutralPlane)
    {
        this.neutralPlane = neutralPlane;
        return this;
    }

    public LevelScore setBadPlane(string badPlane)
    {
        this.badPlane = badPlane;
        return this;
    }

    public LevelScore setTerriblePlane(string terriblePlane)
    {
        this.terriblePlane = terriblePlane;
        return this;
    }
}
