﻿using System.Data;
using Mono.Data.SqliteClient;
using System;
using System.Collections.Generic;
using UnityEngine;

public sealed class DataBaseScoreController
{
    private IDbConnection connection;
    private IDbCommand command;
    private IDataReader reader;

    private string DB_FILE = "URI=File:" +Application.dataPath + "/Plugins/SQLiteDB.db";
    private string TABLE_NAME = "mazelevel";
    private string COLUMN_ID = "id";
    private string COLUMN_LEVEL = "level";
    private string COLUMN_TIME = "time";
    private string COLUMN_CORRECT = "correct";
    private string COLUMN_NEUTRAL= "neutral";
    private string COLUMN_BAD = "bad";
    private string COLUMN_TERRIBLE = "terrible";

    private static readonly DataBaseScoreController instance = new DataBaseScoreController();
    private DataBaseScoreController() { }
    public static DataBaseScoreController Instance
    {
        get
        {
            return instance;
        }
    }

    public void connect()
    {
        connection = new SqliteConnection(DB_FILE);
        command = connection.CreateCommand();
        connection.Open();
        createTableIfNotExists();
    }


    public List<LevelScore> consultAll()
    {
        List<LevelScore> levelScores = new List<LevelScore>();
        string query = "SELECT * FROM " + TABLE_NAME + ";";
        command.CommandText = query;

        reader = command.ExecuteReader();

        while (reader.Read())
        {
            levelScores.Add(new LevelScore()
                .setLevel(reader.GetString(1))
                .setTime(reader.GetString(2))
                .setCorrectPlane(reader.GetString(3))
                .setNeutralPlane(reader.GetString(4))
                .setBadPlane(reader.GetString(5))
                .setTerriblePlane(reader.GetString(6)));
        }
        return levelScores;
    }

    public int getLevel()
    {
        return consultAll().Count;
    }

    public LevelScore consultOne(string level)
    {
        LevelScore levelScore = new LevelScore();
        string query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_LEVEL + " = '"+ level +"';";
        command.CommandText = query;
        reader = command.ExecuteReader();

        while (reader.Read())
        {
            levelScore
                .setLevel(reader.GetString(1))
                .setLevel(reader.GetString(2))
                .setTime(reader.GetString(3))
                .setCorrectPlane(reader.GetString(4))
                .setNeutralPlane(reader.GetString(5))
                .setBadPlane(reader.GetString(6))
                .setTerriblePlane(reader.GetString(7));
        }
        return levelScore;
    }

    public void updateOrInsert()
    {
        int line = countLine();
        int time = getTime();
        
        command.CommandText = getQueryUpdateOrInsert(line, time);
        command.ExecuteNonQuery();
    }

    private string newInsert()
    {
       return "INSERT INTO " + TABLE_NAME + "(" + COLUMN_LEVEL + ","
            + COLUMN_TIME + "," + COLUMN_CORRECT + ","
            + COLUMN_NEUTRAL + "," + COLUMN_BAD + "," + COLUMN_TERRIBLE + ") "
            + " VALUES('" + DesignMaze.levelName + "', '" + MazeInfo.Instance.time
            + "', '" + MazeInfo.Instance.getCorrectPlane() + "', '" + MazeInfo.Instance.getNeutralPlane()
            + "', '" + MazeInfo.Instance.getBadPlane()
            + "', '" + MazeInfo.Instance.getTerriblePlane() + "');";
    }

    private string update()
    {
        return "UPDATE " + TABLE_NAME + " SET " + COLUMN_TIME + " = " 
            + MazeInfo.Instance.time.ToString() + ", " 
            + COLUMN_CORRECT + " = " + MazeInfo.Instance.getCorrectPlane() + ", "
            + COLUMN_NEUTRAL + " =  " + MazeInfo.Instance.getNeutralPlane() + 
            ", " + COLUMN_BAD + " = " + MazeInfo.Instance.getBadPlane() + ", " 
            + COLUMN_TERRIBLE + " = " + MazeInfo.Instance.getTerriblePlane()
            + " WHERE " + COLUMN_LEVEL + " = '" + DesignMaze.levelName + "';";
    }

    public void delete(string level)
    {
        string query = "DELETE FROM " + TABLE_NAME + " WHERE " + COLUMN_LEVEL + " = '" + level + "';"; ;
        command.CommandText = query;
        command.ExecuteNonQuery();
    }

    public void deleteAll()
    {
        string query = "DELETE FROM " + TABLE_NAME + ";";
        command.CommandText = query;
        command.ExecuteNonQuery();
    }

    private string getQueryUpdateOrInsert(int line, int time)
    {
        string query = "";
        if (line == 0)
            query = newInsert();
        else if (time < MazeInfo.Instance.time)
            query = update();
        return query;
    }

    private int getTime()
    {
        int time = 0;
        string query = "SELECT " + COLUMN_TIME + " FROM " + TABLE_NAME + " WHERE " + COLUMN_LEVEL + " = " + DesignMaze.levelName + ";";
        command.CommandText = query;
        reader = command.ExecuteReader();

        while (reader.Read())
        {
            time = Convert.ToInt32(reader.GetString(0));
        }
        return time;
    }

    private int countLine()
    {
        string query = "SELECT COUNT(" + COLUMN_ID + ") FROM " + TABLE_NAME + " WHERE " + COLUMN_LEVEL + " = '" + DesignMaze.levelName + "';";
        command.CommandText = query;
        return Convert.ToInt32(command.ExecuteScalar());
    }

    private void createTableIfNotExists()
    {
        string table = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_LEVEL + " VARCHAR(5) UNIQUE," + COLUMN_TIME + " VARCHAR(10),"
            + COLUMN_CORRECT + " VARCHAR(10)," + COLUMN_NEUTRAL + " VARCHAR(10),"
            + COLUMN_BAD + " VARCHAR(10)," + COLUMN_TERRIBLE + " VARCHAR(10));";
        command.CommandText = table;
        command.ExecuteNonQuery();
    }
}
