﻿using System;
using System.Net;
using System.Net.Mail;

public class SendEmail 
{
    private SmtpClient clientDetails = new SmtpClient();
    private const string email = "mathematicallabyrinth@gmail.com";
    public const string OK = "OK";

    public SendEmail()
    {
        clientDetails.Port = 587;
        clientDetails.Host = "smtp.gmail.com";
        clientDetails.EnableSsl = true;
        clientDetails.DeliveryMethod = SmtpDeliveryMethod.Network;
        clientDetails.Credentials = new NetworkCredential(email, Encryption.Descriptografar("ekKiqMKV/Mow/AZ9kOByIg=="));
    }

    public string send(string email, string subject, string body)
    {
        Attachment attachment = null;
        MailMessage mailDetails = new MailMessage();
        mailDetails.From = new MailAddress(SendEmail.email);
        try
        {
            mailDetails.To.Add(email);
        }
        catch (FormatException ex)
        {
            return "Email invalido";
        }
        catch (Exception ex)
        {
            return "Erro! Entre em contato:" + email;
        }

        try
        {
            attachment = new Attachment(PDFFormatTableGenerator.PATH + PDFFormatTableGenerator.ARCHIVE_NAME + ".pdf");
        }
        catch (Exception ex)
        {
            return "Por favor, gerar Pontuação no Escrivão.";
        }
        mailDetails.Attachments.Add(attachment);
        mailDetails.Subject = subject;
        mailDetails.Body = body;

        
        clientDetails.Send(mailDetails);
        return OK;
    }
   
}
