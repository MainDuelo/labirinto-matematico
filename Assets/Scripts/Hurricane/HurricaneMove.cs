﻿using UnityEngine;

public class HurricaneMove : MonoBehaviour
{
    [SerializeField]
    private GameObject score;
    [SerializeField]
    private Vector3[] currentPoint = new Vector3[6];
    [SerializeField]
    private float speed;
    private int targetIndex = 0;

    private void Start()
    {
        targetIndex = Random.Range(0, currentPoint.Length);
        transform.position = currentPoint[targetIndex];
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, currentPoint[targetIndex], Time.deltaTime * speed);
        if (transform.position == currentPoint[targetIndex])
            targetIndex = (targetIndex + 1) % currentPoint.Length;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(PlayerControll.PLAYER_TAG))
            score.SetActive(true);
    }
}
