﻿using UnityEngine;

public sealed class Reset
{
    private static readonly Reset instance = new Reset();
    private Reset() { }
    public static Reset Instance
    {
        get
        {
            return instance;
        }
    }
   
    public void resetAll()
    {
        deleteAllSavePlayerPrefs();
        deleteAllDataBase();
    }

    private void deleteAllSavePlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }

    private void deleteAllDataBase()
    {
        DataBaseScoreController.Instance.connect();
        DataBaseScoreController.Instance.deleteAll();
    }
}
