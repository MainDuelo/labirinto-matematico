﻿using System.Collections.Generic;

public class Demonstrations
{
    private static List<MathDemonstration> mathDemonstrations = new List<MathDemonstration>();
    private static readonly Demonstrations instance = new Demonstrations();

    public static Demonstrations getInstance()
    {
        return instance;
    }

    public List<MathDemonstration> getList()
    {
        return mathDemonstrations;
    }

    public Demonstrations()
    {

        mathDemonstrations.Add(new MathDemonstration("Propriedades da soma e da multiplicação ")
            .addText("a + b = b + a")
            .addText("(a + b) + c = a + (b + c)")
            .addText("(ab)c = a(bc)")
            .addText("a(b + c) = ab + ac"));

        mathDemonstrations.Add(new MathDemonstration("Função")
            .addText("f(x) = 2x + 6")
            .addText("f(4) = 2x + 6")
            .addText("f(4) = 2.4 + 6")
            .addText("f(4) = 8 + 6")
            .addText("f(4) = 14"));

        mathDemonstrations.Add(new MathDemonstration("Bhaskara")
            .addText("x² + 2x + 1 = 0")
            .addText("a = 1; b = 2; c = 1")
            .addText("Δ = b² -4.a.c")
            .addText("Δ = 2² -4.1.1")
            .addText("Δ = 0")
            .addText("x = (-b ± √Δ) / (2.a)")
            .addText("x = (-2 ± √0) / (2.1)")
            .addText("x' = -1  x'' = -1")
            .addText("(-1,0)  ↑"));

        mathDemonstrations.Add(new MathDemonstration("Equação do 1º Grau")
            .addText("5 + x = 8")
            .addText("-5 + 5 + x = 8 - 5")
            .addText("x = 8 - 5")
            .addText("x = 3")
            );

        mathDemonstrations.Add(new MathDemonstration("Propriedades dos números negativos ")
            .addText("(-1)a = -a")
            .addText("-(-a) = a")
            .addText("(-a)b = a(-b) = -(ab)")
            .addText("(-a)(-b) = ab")
            .addText("-(a + b) = -a -b")
            .addText("-(a - b) = -a +b = b -a")
            .addText("(-a)/b = a/(-b) = -a/b")
            .addText("(-a)/(-b) = a/b"));

        mathDemonstrations.Add(new MathDemonstration("Função")
            .addText("f(x) = xº + 1 + ³√x")
            .addText("f(27) = xº + 1 + ³√x")
            .addText("f(27) = 27º + 1 + ³√27")
            .addText("f(27) = 1 + 1 + 3")
            .addText("f(27) = 5"));

        mathDemonstrations.Add(new MathDemonstration("Bhaskara")
            .addText("x² + 3x - 4 = 0")
            .addText("a = 1; b = 3; c = -4")
            .addText("Δ = b² -4.a.c")
            .addText("Δ = 3² -4.1.-4")
            .addText("Δ = 25")
            .addText("x = (-b ± √Δ) / (2.a)")
            .addText("x = (-3 ± √25) / (2.1)")
            .addText("x' = 1  x'' = -4")
            .addText("(-1.5, -6.25)  ↑"));

        mathDemonstrations.Add(new MathDemonstration("Equação do 1º Grau")
            .addText("9x - 4x + 10 = 7x - 30")
            .addText("9x - 4x - 7x + 10 = 7x - 7x - 30")
            .addText("9x - 4x - 7x + 10 = - 30")
            .addText("9x - 4x - 7x + 10 - 10 = - 10 - 30")
            .addText("9x - 4x - 7x = - 10 - 30")
            .addText("-20x = - 40")
            .addText("x = -40/-20")
            .addText("x = 2")
            );

        mathDemonstrations.Add(new MathDemonstration("Propriedades das frações b;d;k diferente de 0")
            .addText("a/b + c/b = (a + c)/b")
            .addText("a/b - c/b = (a - c)/b")
            .addText("a/b + c/d = (ad + cb)/(bd)")
            .addText("a/b - c/d = (ad - cb)/(bd)")
            .addText("(ad)/(bd) = a/b")
            .addText("a/b . c/d = (ac)/(bd)")
            .addText("a/b / k/d = a/b . d/k = (ad)/(bk)"));

        mathDemonstrations.Add(new MathDemonstration("Função")
            .addText("f(x) = log(x) + xº")
            .addText("f(100) = log(x) + xº")
            .addText("f(100) = log(100) + 64º")
            .addText("f(100) = 2 + 1")
            .addText("f(100) = 3"));

        mathDemonstrations.Add(new MathDemonstration("Bhaskara")
            .addText("-2x² - 2x + 4 = 0")
            .addText("a = -2; b = -2; c = 4")
            .addText("Δ = b² -4.a.c")
            .addText("Δ = -2² -4.-2.4")
            .addText("Δ = 36")
            .addText("x = (-b ± √Δ) / (2.a)")
            .addText("x = (+2 ± √36) / (2.-2)")
            .addText("x' = -2  x'' = 1")
            .addText("(-0.5, 4.5)  ↓"));

        mathDemonstrations.Add(new MathDemonstration("Equação do 1º Grau")
            .addText("23x - 16 = 14 - 17x")
            .addText("23x = 14 - 17x + 16")
            .addText("23x + 17x = 30")
            .addText("40x = 30")
            .addText("x = 30/40")
            .addText("x = 3/4")
            );

        mathDemonstrations.Add(new MathDemonstration("Propriedades das potências ")
            .addText("xªxⁿ = xª+ⁿ")
            .addText("xª/xⁿ = xª-ⁿ")
            .addText("(xª)ⁿ = xªⁿ")
            .addText("(xy)ⁿ = xⁿyⁿ")
            .addText("(x/y)ⁿ = xⁿ/yⁿ")
            .addText("xºⁿ =1"));

        mathDemonstrations.Add(new MathDemonstration("Função")
            .addText("f(x) = |-x| + x⁵ - x")
            .addText("f(2) = |-x| + x⁵ - x ")
            .addText("f(2) = |-2| + 2⁵ - 2 ")
            .addText("f(2) = 2 + 32 - 2")
            .addText("f(2) = 32"));

        mathDemonstrations.Add(new MathDemonstration("Bhaskara")
            .addText("x² + 2x - 15 = 0")
            .addText("a = 1; b = 2; c = -15")
            .addText("Δ = 2² -4.1.-15")
            .addText("Δ = 64")
            .addText("x = (-b ± √Δ) / (2.a)")
            .addText("x = (-2 ± √64) / (2.1)")
            .addText("x' = 3  x'' = -5")
            .addText("(-1, -16)  ↑"));

        mathDemonstrations.Add(new MathDemonstration("Equação do 1º Grau")
            .addText("[2(x - 5) + 4(1 - 2x)] / 20 = 5 (3 - x) / 20")
            .addText("2x - 10 + 4 - 8x = 15 - 5x")
            .addText("-6x - 6 = 15 - 5x")
            .addText("-6x + 5x = 15 + 6")
            .addText("-x = 21")
            .addText("x = -21")
            );


        mathDemonstrations.Add(new MathDemonstration("Propriedades dos expoentes negativos")
            .addText("1/x­-­­ⁿ = xⁿ")
            .addText("x-ª/y-ⁿ = yⁿ/xª")
            .addText("(x/y)-ⁿ = yⁿ/xⁿ")
            );

        mathDemonstrations.Add(new MathDemonstration("Propriedade das raízes")
            .addText("ⁿ√xy = ⁿ√xⁿ√y")
            .addText("ⁿ√(x/y) = ⁿ√x/ⁿ√y")
            .addText("ª√(ⁿ√x) = ªⁿ√x")
            .addText("ⁿ√xⁿ = x, n odd. |x|, n par"));

        mathDemonstrations.Add(new MathDemonstration("Propriedades da união")
            .addText("AUA = A")
            .addText("AUø = A")
            .addText("AUB = BUA")
            .addText("(AUB)UC = AU(BUC)"));

        mathDemonstrations.Add(new MathDemonstration("Propriedades da interseção")
            .addText("A∩A = A")
            .addText("A∩ø = ø")
            .addText("A∩B = B∩A")
            .addText("(A∩B)∩C = A∩(B∩C)"));

        mathDemonstrations.Add(new MathDemonstration("Produtos notáveis")
            .addText("(a + b)² = a² + 2ab + b²")
            .addText("(a - b)² = a² -2ab +b²")
            .addText("(a + b)(a -b) = a² -b²")
            .addText("(a + b)³ = a³ + 3a²b + 3ab² + b³")
            .addText("(a - b)³ = a³ - 3a²b + 3ab² - b³"));

        mathDemonstrations.Add(new MathDemonstration("Propriedades do valor absoluto")
            .addText("|a| ≥ 0")
            .addText("|-a| = |a|")
            .addText("|ab| = |a|.|b|")
            .addText("|a/b| = |a|/|b|")
            .addText("|a+b| ≤ |a| + |b|")
            .addText("|aⁿ| = aⁿ, n par"));

    }
}
