﻿public sealed class MathDemonstrationsController
{
    private static readonly MathDemonstrationsController instance = new MathDemonstrationsController();
    private MathDemonstration currentDemostration;
    private int indexTextCurrentDemostration = 0;
    private int indexCurrentDemostration;

    private MathDemonstrationsController() { }

    public static MathDemonstrationsController Instance
    {
        get
        {
            return instance;
        }
    }

    public void start()
    {
        setCurrentDemostration();
    }

    private void setCurrentDemostration()
    {
        indexCurrentDemostration = UnityEngine.Random.Range(0, Demonstrations.getInstance().getList().Count);
        currentDemostration = Demonstrations.getInstance().getList()[indexCurrentDemostration];
    }

    public string getTitle()
    {
        return currentDemostration.getTitle();
    }

    public string getText()
    {
        if (indexTextCurrentDemostration == currentDemostration.getText().Count)
        {
            indexTextCurrentDemostration = 0;
            return "";
        }
        return currentDemostration.getText()[indexTextCurrentDemostration++];
    }

    public void nextCurrentDemostration()
    {
        indexCurrentDemostration++;
        if (indexCurrentDemostration == Demonstrations.getInstance().getList().Count)
            indexCurrentDemostration = 0;

        currentDemostration = Demonstrations.getInstance().getList()[indexCurrentDemostration];
    }
}
