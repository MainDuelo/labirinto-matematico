﻿using TMPro;
using UnityEngine;

public class DemonstrationFramework : MonoBehaviour {

    private MathDemonstrationsController mathDemonstrationsController = MathDemonstrationsController.Instance;

    private TextMeshPro demoText;

    private float time;

    private string text = "#";

    private void Start ()
    {
        demoText = GetComponent<TextMeshPro>();
        mathDemonstrationsController.start();
        demoText.text += mathDemonstrationsController.getTitle();
    }
	
	private void Update ()
    {
        if(time > 3)
        {
            if (text.Equals(""))
            {
                demoText.text = "";
                mathDemonstrationsController.nextCurrentDemostration();
                demoText.text += mathDemonstrationsController.getTitle();
            }

            text = mathDemonstrationsController.getText();
            demoText.text += "\n" + text;
            time = 0;    
        }
        time += Time.deltaTime;
    }
}
