﻿using UnityEngine;

public class AutoRotationTextMeshPro : MonoBehaviour {

    private Transform cam;

    private void Start ()
    {
        cam = Camera.main.transform;
    }

    private void Update () {
        transform.eulerAngles = new Vector3(0, cam.transform.localEulerAngles.y, 0);
	}
}
