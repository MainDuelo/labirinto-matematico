﻿using System.Collections.Generic;


public class MathDemonstration {

    private string title;
    private List<string> text = new List<string>();

    public MathDemonstration(string title)
    {
        this.title = title;
    }

    public MathDemonstration addText(string text)
    {
        this.text.Add(text);
        return this;
    }

    public string getTitle()
    {
        return title;
    }

    public List<string> getText()
    {
        return text;
    }
}
