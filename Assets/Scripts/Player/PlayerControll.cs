﻿using UnityEngine;

public class PlayerControll : MonoBehaviour {

    public const string PLAYER_TAG = "Player";

    private const string AXIS_HORIZONTAL = "Horizontal";
    private const string AXIS_VERTICAL = "Vertical";

    private Transform cam;
    [SerializeField]
    public float speed = 4f;
    [SerializeField]
    private float turnSpeed = 9f;

    Rigidbody rigiBody;

    int skillVelocityUp = 0;
    bool limitedSkillVelocityUp = false;
    float returnSkillVelocityUp = 6;

	void Start ()
    {
        rigiBody = GetComponent<Rigidbody>();
        cam = Camera.main.transform;
        rigiBody.freezeRotation = true;
	}

    private void FixedUpdate()
    {
        Vector3 direction = (cam.right * Input.GetAxis(AXIS_HORIZONTAL)) + (cam.forward * Input.GetAxis(AXIS_VERTICAL));

        direction.y = 0;
        
        if (Input.GetAxis(AXIS_HORIZONTAL) != 0 || Input.GetAxis(AXIS_VERTICAL) != 0)
        {
            rigiBody.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), turnSpeed * Time.deltaTime);
            rigiBody.velocity = transform.forward * speed;
            PlayerAnimator.walk();
        }
        else
        {
            PlayerAnimator.stand();
        }
    }
}
