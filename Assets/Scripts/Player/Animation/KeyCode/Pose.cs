﻿using UnityEngine;

public class Pose : KeyCodeAnimation
{
    public void action()
    {
        PlayerAnimator.pose();
    }

    public bool checkKey(KeyCode keycode)
    {
        return keycode == KeyCode.Space;
    }
}