﻿using UnityEngine;

public interface KeyCodeAnimation
{
    bool checkKey(KeyCode keycode);

    void action();
}