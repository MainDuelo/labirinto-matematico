﻿using UnityEngine;

public class Attack03 : KeyCodeAnimation
{
    public void action()
    {
        PlayerAnimator.attack03();
    }

    public bool checkKey(KeyCode keycode)
    {
        return keycode == KeyCode.K;
    }
}