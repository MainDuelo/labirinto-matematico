﻿using UnityEngine;

public class Attack02 : KeyCodeAnimation
{
    public void action()
    {
        PlayerAnimator.attack02();
    }

    public bool checkKey(KeyCode keycode)
    {
        return keycode == KeyCode.J;
    }
}