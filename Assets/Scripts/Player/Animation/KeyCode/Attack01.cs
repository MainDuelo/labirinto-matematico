﻿using UnityEngine;

public class Attack01 : KeyCodeAnimation {
    public void action()
    {
        PlayerAnimator.attack01();
    }

    public bool checkKey(KeyCode keycode)
    {
        return keycode == KeyCode.H;
    }
}
