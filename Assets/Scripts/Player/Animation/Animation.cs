﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Animation : MonoBehaviour {

    private List<KeyCodeAnimation> keyCodeAnimation = new List<KeyCodeAnimation>();

    void Start()
    {
        Attack01 attack01 = new Attack01();
        Attack02 attack02 = new Attack02();
        Attack03 attack03 = new Attack03();
        Pose pose = new Pose();
        keyCodeAnimation.Add(attack01);
        keyCodeAnimation.Add(attack02);
        keyCodeAnimation.Add(attack03);
        keyCodeAnimation.Add(pose);
    }

    private void Update()
    {
        if (Input.anyKeyDown)
        {
            foreach (KeyCode keyCode in Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKeyDown(keyCode))
                {
                    foreach (KeyCodeAnimation rule1 in keyCodeAnimation)
                    {
                        if (rule1.checkKey(keyCode))
                            rule1.action();
                    }
                }
            }
        }
    }
}
