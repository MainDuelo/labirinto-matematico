﻿using UnityEngine;

public class PlayerAnimator : MonoBehaviour {

    public static Animator animator;

    private void Awake () {
        animator = GetComponent<Animator>();
    }
    
    public static void attack01()
    {
        animator.SetTrigger("attack_1");
    }

    public static void attack02()
    {
        animator.SetTrigger("attack_2");
    }

    public static void attack03()
    {
        animator.SetTrigger("attack_3");
    }

    public static void pose()
    {
        animator.SetTrigger("pose");
    }

    public static void walk()
    {
        animator.ResetTrigger("stand");
        animator.Play("priest_walk");
    }

    public static void stand()
    {
        animator.ResetTrigger("walk");
        animator.SetTrigger("stand");
    }

    public static void damage()
    {
        animator.SetTrigger("damage");
    }

    public static void stun()
    {
        animator.SetTrigger("stun");
    }

    public static void down()
    {
        animator.SetTrigger("down");
    }

    public static void death()
    {
        animator.SetTrigger("death");
    }

    public static bool noStand()
    {
        return animatorIsPlaying("priest_walk");
    }

    private static bool animatorIsPlaying()
    {
        return animator.GetCurrentAnimatorStateInfo(0).length >
               animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }

    public static bool animatorIsPlaying(string stateName)
    {
        return animatorIsPlaying() && animator.GetCurrentAnimatorStateInfo(1).IsName(stateName);
    }

}
