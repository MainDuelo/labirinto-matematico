﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Skin : MonoBehaviour
{
    [SerializeField]
    private GameObject skin01, skin02, skin03, skin04;
    [SerializeField]
    private GameObject currentSkin;
    public bool isDialogue = false;

    
    public static Skin Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Update()
    {
        string nameScene = SceneManager.GetActiveScene().name;
        DataBaseScoreController.Instance.connect();
        int level = DataBaseScoreController.Instance.getLevel();
        if(nameScene.Equals("Home"))
            if (Input.GetKeyDown(KeyCode.Alpha1) && !isDialogue)
            {
                currentSkin = skin01;
                Destroy();
                Instantiate(currentSkin, new Vector3(15, 0.1f, 10), Quaternion.identity);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2) && level > 0 && !isDialogue)
            {
                currentSkin = skin02;
                Destroy();
                Instantiate(currentSkin, new Vector3(15, 0.1f, 10), Quaternion.identity);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3) && level >2 && !isDialogue)
            {
                currentSkin = skin03;
                Destroy();
                Instantiate(currentSkin, new Vector3(15, 0.1f, 10), Quaternion.identity);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4) && level > 5 && !isDialogue)
            {
                currentSkin = skin04;
                Destroy();
                Instantiate(currentSkin, new Vector3(15, 0.1f, 10), Quaternion.identity);
            }
    }

    private void Destroy()
    {
        Destroy(GameObject.Find("Pagan_A_SKEL(Clone)"));
        Destroy(GameObject.Find("Pagan_B_SKEL(Clone)"));
        Destroy(GameObject.Find("Pagan_C_SKEL(Clone)"));
        Destroy(GameObject.Find("Pagan_SKEL(Clone)"));
    }

    public GameObject getCurrentSkin()
    {
        return currentSkin;
    }
}
