﻿using UnityEngine;

public class CameraControll : MonoBehaviour {

    private const float Y_ANGLE_MIN = -50.0f;
    private const float Y_ANGLE_MAX = 10.0f;
    private const string MOUSE_SCROLLWHEEL = "Mouse ScrollWheel";

    public Transform character;
    [SerializeField]
    private float distance;
    [SerializeField]
    private float distanceMax;
    [SerializeField]
    private float distanceMin;

    private float currentX = 0.0f;
    private float currentY = 0.0f;

    private void Start()
    {
        character = GameObject.Find("PLACE_VIEW").transform;
    }

    private void Update ()
    {
        currentX += Input.GetAxis("Mouse X");
        currentY += Input.GetAxis("Mouse Y");

        currentY = Mathf.Clamp(currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);
        AlterDistance();
    }

    private void LateUpdate()
    {
        if(character != null)
        {
            gameObject.transform.position = character.position + Quaternion.Euler(currentY + 10, currentX, 0) * new Vector3(0, 0, distance);
            gameObject.transform.LookAt(character.position);
        }
        else
        {
            findChareacter();
        }
    }

    private void AlterDistance()
    {
        if (Input.GetAxis(MOUSE_SCROLLWHEEL) > 0f && distance <= distanceMax)
        {
            distance += +0.1f;
        }
        else if (Input.GetAxis(MOUSE_SCROLLWHEEL) < 0f && distance > distanceMin)
        {
            distance += -0.1f;
        }
    }

    private void findChareacter()
    {
        GameObject _object = GameObject.Find("PLACE_VIEW");
        if (_object != null)
        {
            distance = 1;
            character = _object.gameObject.transform;
        }
    }
}
