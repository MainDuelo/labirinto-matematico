﻿using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

public class DaemonCommunication : MonoBehaviour
{
    [SerializeField]
    private GameObject dialogue, currentCamera;
    [SerializeField]
    private Button buttonGeneratePDF, buttonOpenPDF, buttonExit;
    private Collider player;

    private void Start()
    {
        buttonGeneratePDF.onClick = new Button.ButtonClickedEvent();
        buttonGeneratePDF.onClick.AddListener(() => clickGeneratePDF());
        buttonOpenPDF.onClick = new Button.ButtonClickedEvent();
        buttonOpenPDF.onClick.AddListener(() => clickOpenPDF());
        buttonExit.onClick = new Button.ButtonClickedEvent();
        buttonExit.onClick.AddListener(() => clickExit());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PlayerControll.PLAYER_TAG))
        {
            Skin.Instance.isDialogue = true;
            startDialogue();
            other.attachedRigidbody.constraints = RigidbodyConstraints.FreezeAll;
            currentCamera.GetComponent<CameraControll>().enabled = false;
            currentCamera.transform.SetPositionAndRotation(new Vector3(25, 2, 10f), Quaternion.Euler(15, 90f, 0));
            player = other;
        }
    }

    private void startDialogue()
    {
        dialogue.SetActive(true);
    }

    private void clickGeneratePDF()
    {
        generateScorePDF();
        liberatePlayer();
        dialogue.SetActive(false);
        
    }

    private void clickOpenPDF()
    {
        Process.Start("explorer.exe", PDFFormatTableGenerator.PATH.Replace("/", "\\"));
        dialogue.SetActive(false);
        liberatePlayer();
    }

    private void clickExit()
    {
        dialogue.SetActive(false);
        liberatePlayer();
    }

    private void liberatePlayer()
    {
        player.attachedRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        currentCamera.GetComponent<CameraControll>().enabled = true;
        Skin.Instance.isDialogue = false;
    }

    private void generateScorePDF()
    {
        PDFFormatTableGenerator pDFFormatTableGenerator = new PDFFormatTableGenerator(); 
        pDFFormatTableGenerator.setNumberColumns(6);
        pDFFormatTableGenerator.setNameUser(MenuStartGame.NAME_USER, PDFFormatTableGenerator.fontUserName);
        pDFFormatTableGenerator.setFirstLine(PDFFormatTableGenerator.getColumns());
        pDFFormatTableGenerator.setScoreTable(DataBaseScoreController.Instance.consultAll(), PDFFormatTableGenerator.font);
        pDFFormatTableGenerator.generateTable();
    }
}