﻿using UnityEngine;
using UnityEngine.UI;

public class BugCommunication : MonoBehaviour
{
    [SerializeField]
    private GameObject insect;
    [SerializeField]
    private GameObject dialogue, panelEmail, currentCamera;
    [SerializeField]
    private Button buttonYes, buttonNot, buttonSend, buttonClose;
    [SerializeField]
    private InputField inputFieldEmail, inputFieldSubject, inputFieldBody;
    [SerializeField]
    private Text textError;
    private Collider player;

    private void Start()
    {
        buttonYes.onClick = new Button.ButtonClickedEvent();
        buttonYes.onClick.AddListener(() => clickYes());
        buttonNot.onClick = new Button.ButtonClickedEvent();
        buttonNot.onClick.AddListener(() => clickNot());
        buttonSend.onClick = new Button.ButtonClickedEvent();
        buttonSend.onClick.AddListener(() => clickSend());
        buttonClose.onClick = new Button.ButtonClickedEvent();
        buttonClose.onClick.AddListener(() => clickClose());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PlayerControll.PLAYER_TAG))
        {
            Skin.Instance.isDialogue = true;
            startDialogue();
            other.attachedRigidbody.constraints = RigidbodyConstraints.FreezeAll;
            currentCamera.GetComponent<CameraControll>().enabled = false;
            currentCamera.transform.SetPositionAndRotation(new Vector3(3.7f, 2, 17.5f), Quaternion.Euler(15,-45f,0));
            player = other;
        } 
    }

    private void startDialogue()
    {
        backInsect();
        dialogue.SetActive(true);
    }

    private void clickYes()
    {
        dialogue.SetActive(false);
        panelEmail.SetActive(true);
    }

    private void clickNot()
    {
        dialogue.SetActive(false);
        liberatePlayer();
    }

    private void clickSend()
    {
        SendEmail sendEmail = new SendEmail();
        string error = sendEmail.send(inputFieldEmail.text, inputFieldSubject.text, inputFieldBody.text);
        possibleError(error);
    }

    private void clickClose()
    {
        dialogue.SetActive(false);
        panelEmail.SetActive(false);
        liberatePlayer();
    }

    private void possibleError(string error)
    {
        if (error.Equals(SendEmail.OK))
        {
            liberatePlayer();
            panelEmail.SetActive(false);
            sendInsect();
            clearInputFields();
        }
        else
        {
            textError.text = error;
        }
    }

    private void liberatePlayer()
    {
        player.attachedRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        currentCamera.GetComponent<CameraControll>().enabled = true;
        Skin.Instance.isDialogue = false;

    }

    private void clearInputFields()
    {
        inputFieldEmail.text = "";
        inputFieldSubject.text = "";
        inputFieldBody.text = "";
        textError.text = "";
    }

    private void sendInsect()
    {
        insect.GetComponent<Insect>().send = true;
        insect.GetComponent<Insect>().setAnimatorFlying();
    }

    private void backInsect()
    {
        insect.GetComponent<Insect>().send = false;
        insect.GetComponent<Insect>().setAnimatorIdle();
        insect.GetComponent<Insect>().setInitialPosition();
    }
}
