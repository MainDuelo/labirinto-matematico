﻿using UnityEngine;

public class Insect : MonoBehaviour
{
    private Animator animator;
    [SerializeField]
    private Vector3 targetPosition = new Vector3(100, 100, 20);
    [SerializeField]
    private float speed = 2f;
    private Vector3 initialPosition;

    public bool send = false;

    private void Start()
    {
        initialPosition = transform.position;
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (send)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * speed);
            if (transform.position == targetPosition)
                setInitialPosition();
        }   
    }

    public void setAnimatorIdle()
    {
        animator.Play("Idle", 0);
    }

    public void setAnimatorFlying()
    {
        animator.Play("Flying", 0);
    }

    public void setInitialPosition()
    {
        transform.position = initialPosition;
    }
}
