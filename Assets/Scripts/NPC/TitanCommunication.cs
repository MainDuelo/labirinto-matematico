﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitanCommunication : MonoBehaviour
{
    [SerializeField]
    private GameObject dialogue, currentCamera;
    [SerializeField]
    private Button buttonYes, buttonNot;
    private Collider player;

    private void Start()
    {
        buttonYes.onClick = new Button.ButtonClickedEvent();
        buttonYes.onClick.AddListener(() => clickYes());
        buttonNot.onClick = new Button.ButtonClickedEvent();
        buttonNot.onClick.AddListener(() => clickNot());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PlayerControll.PLAYER_TAG))
        {
            Skin.Instance.isDialogue = true;
            startDialogue();
            other.attachedRigidbody.constraints = RigidbodyConstraints.FreezeAll;
            currentCamera.GetComponent<CameraControll>().enabled = false;
            currentCamera.transform.SetPositionAndRotation(new Vector3(25.3f, 3, 25f), Quaternion.Euler(20, 40f, 0));
            player = other;
        }
    }

    private void startDialogue()
    {
        dialogue.SetActive(true);
    }

    private void clickYes()
    {
        Reset.Instance.resetAll();
        liberatePlayer();
        dialogue.SetActive(false);
        SceneManager.LoadScene("Home");
    }

    private void clickNot()
    {
        dialogue.SetActive(false);
        liberatePlayer();
    }

    private void liberatePlayer()
    {
        player.attachedRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        currentCamera.GetComponent<CameraControll>().enabled = true;
        Skin.Instance.isDialogue = false;
    }
}
